import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {Hotel} from './hotel.model';


@Pipe({
  name: 'price'
})
@Injectable()
export class PricePipe implements PipeTransform {
  transform(hotels: Hotel[], range: number[]) {
    if (!hotels || !range) {
      return hotels;
    }
    return hotels.filter(some => some.price <= range[1] && some.price >= range[0]);
  }
}
