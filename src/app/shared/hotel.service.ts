import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class HotelService {
  constructor(private http: HttpClient) {
  }
  getHotels() {
    return this.http.get('http://localhost:3000/api/hotels');
  }
}
