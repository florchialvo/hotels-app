import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {Hotel} from './hotel.model';


@Pipe({
  name: 'name'
})
@Injectable()
export class NamePipe implements PipeTransform {
  transform(hotels: Hotel[], name: string) {
    if (!hotels || !name) {
      return hotels;
    }
    return hotels.filter(some => some.name.toLowerCase().includes(name.toLowerCase()));
  }
}
