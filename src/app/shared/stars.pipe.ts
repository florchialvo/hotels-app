import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {Hotel} from './hotel.model';


@Pipe({
  name: 'stars',
  pure: false
})
@Injectable()
export class StarsPipe implements PipeTransform {
  transform(hotels: Hotel[], stars: number[]) {
   if (!hotels || !stars || stars.length === 0) {
      return hotels;
    }
    console.log(stars);
    return hotels.filter(some => stars.includes(some.stars));
  }
}
