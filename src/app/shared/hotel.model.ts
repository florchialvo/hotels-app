export class Hotel {
  name: string;
  price: number;
  stars: number;
}
