import {Component, OnInit} from '@angular/core';
import {HotelService} from './shared/hotel.service';
import {Hotel} from './shared/hotel.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit  {
  hotels: Hotel[];
  name = '';
  price = 0;
  stars =  [];
  someRange = [0, 6000];


  constructor(private hotelService: HotelService) { }

  ngOnInit(): void {
   this.hotelService.getHotels().subscribe(data => {
     this.hotels = data['hotels'];
   });
  }

  onChange(star: number, isChecked: boolean) {
    if (isChecked) {
      this.stars.push(star);
    } else {
      const index = this.stars.indexOf(star);
      this.stars.splice(index, 1);
    }
  }

}
