import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HotelService} from './shared/hotel.service';
import {HttpClientModule} from '@angular/common/http';
import {StarRatingModule} from 'angular-star-rating';
import {NamePipe} from './shared/name.pipe';
import {PricePipe} from './shared/price.pipe';
import {StarsPipe} from './shared/stars.pipe';
import {FormsModule} from '@angular/forms';
import { NouisliderComponent } from 'ng2-nouislider';

@NgModule({
  declarations: [
    AppComponent,
    NamePipe,
    PricePipe,
    StarsPipe,
    NouisliderComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, StarRatingModule.forRoot(), FormsModule
  ],
  providers: [HotelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
